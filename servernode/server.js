var express = require('express');
var app = express();
var fs = require("fs");


// Definir a route principal
app.get('/', function(req, res) {
  res.send('RESTful API - Pplware');
});

app.get('/listUsers', function (req, res) {
//Read file.json 
   fs.readFile( __dirname + "/" + "file.json", 'utf8', function (err, data) {
       console.log( data );
       res.end( data );
   });
})

app.get('/user/:id', function (req, res) {
   // First read existing users.
   fs.readFile( __dirname + "/" + "file.json", 'utf8', function (err, data) {
       users = JSON.parse( data );
       var user = users["user" + req.params.id] 
       console.log( user );
       res.end( JSON.stringify(user));
   });
})

var id = 2;

app.delete('/deleteUser', function (req, res) {

   // First read existing users.
   fs.readFile( __dirname + "/" + "users.json", 'utf8', function (err, data) {
       data = JSON.parse( data );
       delete data["user" + 2];
       
       console.log( data );
       res.end( JSON.stringify(data));
   });
})


var server = app.listen(8081, function () {

  var host = server.address().address
  var port = server.address().port

  console.log("Example app listening at http://%s:%s", host, port)

})