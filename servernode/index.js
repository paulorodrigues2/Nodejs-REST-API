var express = require("express");
var app = express();
var router = express.Router();
var path = __dirname + '/view/';


var otherFile = require('./database.js') // the .js is optional


router.use(function (req,res,next) {
  console.log("/" + req.method);
  next();
});

router.get("/",function(req,res){
  res.sendFile(path + "index.html");
});

router.get("/about",function(req,res){
  res.sendFile(path + "about.html");
});


router.get("/contact",function(req,res){
  res.sendFile(path + "contact.html");
});

//Define rout for login

app.get("/login", function (req, res) {
  res.sendfile(path + "login.html");
});

app.get("/register", function (req, res) {
  res.sendfile(path + "register.html");
});

router.get("/homepage",function(req,res){
  res.sendfile(path + "homepage.html");
});

app.get("/house", function (req, res) {
  res.sendfile(path + "house.html");
});


app.use("/",router);

app.use("*",function(req,res){
  res.sendFile(path + "404.html");
});




app.listen(3001,function(){
  console.log("Live at Port 3000");
});

