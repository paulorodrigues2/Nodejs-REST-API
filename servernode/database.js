var express    = require("express");
var mysql      = require('mysql');
var app = express();
var async = require('async');

var session = require('express-session');

app.set('views', __dirname + '/view');
app.set('view engine', 'ejs'); //extension of views
var bodyParser = require('body-parser');

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json());


app.use(session({secret: "Shh, its a secret!"}));

//mysql 

var mysql = require('mysql');

var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : '',
  database : 'hosting',
  multipleStatements:true
});



connection.connect(function(err){
if(!err) {
    console.log("Database is connected ... nn");    
} else {
    console.log("Error connecting database ... nn");    
}
});


/*
req is an object containing information about the HTTP request that raised the event. 
In response to req, you use res to send back the desired HTTP response.
*/
app.get("/person/get-all",function(req,res){
//Use connection to execute query and use rows from values
connection.query('SELECT * from person', function(err, rows, fields) {
connection.end();
  if (!err)
  {
    console.log('The solution is: ', rows);
	
  }else
  {
    console.log('Error while performing Query.');
  }
  });
});

app.get("/house/get-all",function(req,res){
//Use connection to execute query and use rows from values
connection.query('SELECT * from house', function(err, rows, fields) {

  if (!err)
  {
  //Send values rows in stringify form JSON
	res.json({type: true, data: JSON.stringify(rows)});
	console.log('The solution is: ', rows);
  }else
  {
  
    console.log('Error while performing Query.');
  }
  });
});

app.use(bodyParser.urlencoded({ extended: true })); 

app.get('/search',function(req,res){
//req.query is for Get form 
	var name = req.query.name;
	
	var QuerySelect = "SELECT * from house where house.name = '"+name+"'";
	console.log(QuerySelect);
connection.query(QuerySelect,function(err, rows, fields) {
 connection.end();
if (err) console.log(err);
var data=[];

var value = rows.length;

	console.log("values of rows " + value);
	if(value > 0 )
	{
		for(i=0;i<value;i++)
		{
//Insert in array rows with name, description, localization and price
			data.push(rows[i].name);
			data.push(rows[i].description);
			data.push(rows[i].localization);
			data.push(rows[i].price);
		}
		res.end(JSON.stringify(data));
		console.log('Send value of search');
	}else{
			res.send('Dont find nothing with that search');
		}
});
});

app.get('/login', function(req, res){

	var email = req.query.email; // Get['email'];
	var password = req.query.password; // Get['password'];
	

	var queryStringLogin = "Select * from person where person.email='"+email+"'"+"and person.password='"+password+"'";
	console.log(queryStringLogin);
	connection.query(queryStringLogin, function(err, rows, fields){
	var value = rows.length;
	
	console.log(value + "Number of rows");
	
	
	
	if(err)
	{
		res.send('Error in login'+err);
		console.log(err);
	}else{
		
		if(value > 0)
		{
		
		for(i=0;i<value;i++)
		{
//Insert in array rows with name, description, localization and price
			
			var id = rows[i].id;
		
			
		}
		
		req.session.userId = id;
		connection.end();
		console.log(id);
		
		//return res.send('<p> User Homepage : <a href="/homePage">View here</a>');
		//res.send('You are redirected for homePage');
		//
		//Redirect with id
		return res.redirect("http://localhost:3001/homepage/");
		
		
		}else{
		//Send message for client side
			return res.send('error in login');
			
		}
	}
	
	});

});

//Get session of the email user ;;;;;
app.get('/main', function(req, res){
	
	res.send(req.session.userId);

});
 


//// Insert likes in House
app.get('/likes/:id', function(req, res){
	
	//parameters
	var id = req.params.id;
	//var userId = req.session.userId;
	
	
  // var sqlFindIfExist = "select * from likes where likes.user_id='"+userId+"' and likes.house_id='"+id+"'";
  // var sqlNewLike = "insert into likes (user_id, house_id) values ('"+userId+"','"+id+"')";
   var sqlUserId = "update house set likes = likes + 1 where id='"+id+"'";
   
   

   

   
	 connection.query(sqlUserId, function(err, rows, fields) {
    if(err) console.log(err);
	
	
	/**

 if(rows.length==0) {
		connection.query(sqlNewLike, function(err, rows, fields) {
            if(err) console.log(err);
			
            console.log("added likes");
            //var user_id = results.insertId;
            connection.query(sqlUserId, function(err, rows, fields) {
                if(err) throw err;
				console.log("update likes")
                connection.end();
				return res.redirect("http://localhost:3001");
                });
        });
    }


**/	connection.end();
	return res.redirect("http://localhost:3001");

   
});

});

	

app.post('/insert', function(req,res){
    
  //req.body is for POST form 
  var username = req.body.username;
  var sobrename = req.body.sobrename;
  var email = req.body.email;
  var pais = req.body.pais;
  var datebirth = req.body.datebirth;
  var password = req.body.password;
  
  console.log(username);
       
       var queryString = "insert into person(name,sobrename,email,birthday, country, password) values('"+username+"','"+sobrename+"','"+email+"','"+pais+"','"+datebirth+"','"+password+"')";
        console.log(queryString);
	   
       connection.query(queryString,function(error,results){
	   connection.end();
           if(error)
               {
                   console.log('Error while performing Query FOR INSERT.');
				   connection.end();
				   
               }
           else 
               {
			   console.log('inserted person');
					
                 res.send('Inserted Successfully!')
               }
           
       });
       
  
    
    
});

//Get parameter of url link
/*
// GET /p/5
// tagId is set to 5

app.get('/p/:tagId', function(req, res) {
  res.send("tagId is set to " + req.params.tagId);
});

*//////////

/*
// GET /p?tagId=5
// tagId is set to 5

app.get('/p', function(req, res) {
  res.send("tagId is set to " + req.query.tagId);
});

*///////////





/// Working fine, receive values from house/id  int id;
app.get('/house/:id', function(req, res) {
	var id = req.params.id;
	
	req.session.id = id;
		
	var QuerySelect = "SELECT * from house where house.id = '"+id+"'";
	console.log(QuerySelect);
connection.query(QuerySelect,function(err, rows, fields) {
 
if (err) console.log(err);
var data=[];

var value = rows.length;

	console.log("values of rows " + value);
	if(value > 0 )
	{
		for(i=0;i<value;i++)
		{
//Insert in array rows with name, description, localization and price
			data.push(rows[i].id);
			data.push(rows[i].name);
			data.push(rows[i].description);
			data.push(rows[i].localization);
			data.push(rows[i].price);
		}
		//show JSON data in stringify format // res.end(JSON.stringify(data));
		  //Send values rows in stringify form JSON
		//res.json({type: true, data: JSON.stringify(data)});
		//
		connection.end();
		return res.end(JSON.stringify(data));
		//return res.redirect("http://localhost:3001/homepage/");
	
		//
	
	}else{
			connection.end();
			res.send('Dont find nothing with that search');
		}
		});
		
	
});






app.listen(3000);



